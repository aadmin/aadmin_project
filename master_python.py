import tarfile
import MySQLdb
import datetime
import os
import gzip
import string
import sys
import shutil
import re
#~ commit in master with this line 
#~ import subprocess
#~ import psutil

#~ db = MySQLdb.connect("energy-ide2-cluster.cluster-cym7uz9qnf7d.us-east-1.rds.amazonaws.com","loguser","log@123","loganalyser") # replace your servername username password and dbname here 
db = MySQLdb.connect("localhost","root","blaze.ws","log_analyser") 
cursor = db.cursor()

process_subtype = "";
processtype = "";
parsing_log = "";
parsing_error = "";
file_name = "";
filepath = "";
extract_log = ""
extract_error = "";
extractfilename="";
servername = "";

def find_between( s, first, last ):
	try:
		start = s.index( first ) + len( first )
		end = s.index( last, start )
		return s[start:end]
	except ValueError:
		return ""

#~ gets user_id and job_id from the python arguments globally
user_id = "4";
job_id = "300";

#~ finds the input directory to get the text file with extracted data
input_dir = "/var/www/html/python/collector/pyparsing/"

#~ Chooses where to extract the gz file in our local
output_dir = "/var/www/html/python/collector/pyparsing/extracted/" 

all=string.maketrans('','')
nodigs=all.translate(all, string.digits)
heading = ""

def NumberOnly(myinputstring):
	return re.sub("[^0123456789\.]","",myinputstring)
	
def job_logs_update(log,error, processtype,filename,filepath,extractfile):
	insert = "INSERT INTO logs (user_id,job_id,processtype,logmsg,log_filename,log_filepath,error_code,tar_filename,py_command) VALUES ('"+str(user_id)+"',"+job_id+",'"+str(processtype)+"','"+str(log)+"','"+str(filename)+"','"+str(filepath)+"','"+str(error)+"','"+str(extractfile)+"','""')"
	try:
		cursor.execute(insert)
		db.commit()
	except:
		db.rollback()
			
def extract_file():
	global extract_log , extract_error , file_name , filepath , processtype , extractfilename;
	for filename in os.listdir(input_dir):
		try:
			extractfilename += filename+" \n";
			extract_log += "Extract tar files started "+filename+" \n"
			processtype = "Extract file tar.gz";
			if ("tunbug" in filename and filename.endswith("tar.gz")):
				tar = tarfile.open(input_dir + "/" + filename, "r:gz")
				tar.extractall(output_dir)
				tar.close()
			elif ("tunbug" in filename and filename.endswith("tar")):
				print "This is the tar file extraction"
				tar = tarfile.open(input_dir + "/" + filename, "r:")
				tar.extractall(output_dir)
				tar.close()
		except:
			err = re.sub('[^a-zA-Z0-9.]', ' ', str(sys.exc_info()[1]))
			extract_error+="Extraction failed : "+err+"\n";
			extractfilename += filename+" \n";
			processtype = "Extract file tar.gz";
	job_logs_update(extract_log,extract_error,processtype,filename,filepath,extractfilename)
	
	pid_list=[]
	for filename in os.listdir(output_dir):
		if filename.endswith("log.gz"):
			os.chmod(output_dir + filename,777);
			try:
				#~ os.system("/usr/bin/nohup /usr/bin/python2.7 /var/www/html/python/collector/pyparsing/parsing_process.py "+str(output_dir+filename)+" "+str(filename)+" "+user_id+" "+job_id+"  > /var/www/html/python/collector/pyparsing/extracted/parsing_output.txt 2 >/dev/null &")
				
				
				os.system("/usr/bin/nohup /usr/bin/python2.7 /var/www/html/python/collector/pyparsing/parsing_process.py "+input_dir+" "+output_dir+" "+str(output_dir+filename)+" "+str(filename)+" "+user_id+" "+job_id+" > /var/www/html/python/collector/pyparsing/extracted/parsing_output.txt &")
				
				#~ argument = input_dir +' '+ output_dir +' '+ str(output_dir+filename) +' '+ str(filename) +' '+ user_id +' '+ job_id
				
				#~ command = subprocess.Popen(['/usr/bin/python2.7', '/var/www/html/python/collector/pyparsing/parsing_process.py '+input_dir, output_dir, str(output_dir+filename) ,str(filename) ,user_id ,job_id], shell=True)
				#~ command = subprocess.Popen("/usr/bin/python2.7 /var/www/html/python/collector/pyparsing/parsing_process.py " + argument , shell=True, stdout=subprocess.PIPE)
				#~ procid = command.pid
				#~ print procid
				#~ p = psutil.Process(procid)
				
				extract_log += "* "+filename+" read successfully \n";
				file_name += filename+" \n";
				filepath += output_dir + filename+"\n";
				processtype = "Extract file log.gz";
			except:
				err = re.sub('[^a-zA-Z0-9.]', ' ', str(sys.exc_info()[1]))
				extract_error = "Extract file failed : "+err+"\n";
				processtype = "Extract file log.gz";
		else:
			continue
	job_logs_update(extract_log,extract_error,processtype,file_name,filepath,extractfilename)
	
	#~ pid_list.append(p)
	#~ for i in range(len(pid_list)):
		#~ if pid_list[i].status != 'running':
			#~ print 'ok'
		#~ else:
			#~ print(pid_list[i]).status
		
def trunc_tables():
	sql = "TRUNCATE logs;TRUNCATE tunbug_90_wanbuffers;TRUNCATE logger_xg_2;TRUNCATE citrix_stats;TRUNCATE conn_stats;TRUNCATE tunbug_l0;TRUNCATE ssl_extension_statistics;TRUNCATE tunbug710_inbound;TRUNCATE tunbug710_outbound;TRUNCATE ssl_accelerator_statistics;TRUNCATE packet_miscellaneous_statistics;TRUNCATE logger_5p;TRUNCATE proc_mem;TRUNCATE topres;TRUNCATE vmstat;TRUNCATE disk_usage;TRUNCATE logger_xg;TRUNCATE logger_xf;TRUNCATE meterflow_statistics;TRUNCATE flow_redirection_statistics;TRUNCATE cflow_flows_blob_allocator;TRUNCATE cluster_statistics;TRUNCATE flow_statistics;TRUNCATE packet_fragmentation_statistics;"
	cursor.execute(sql)
	db.commit()
			

#~ trunc_tables()
#~ renamefile()
#~ saveline(heading)
extract_file()

#~ jobs_update()

#~ py_shellcommand()   
#process_mem_insert()
#top_res_insert()
#diskusage_insert()
#vmstat_insert()
#jobs_update()

